﻿using DockerSqlServer.Managers;
using DockerSqlServer.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DockerSqlServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotesController
    {
        private IWebHostEnvironment _hostingEnvironment;

        private readonly AppDbContext _db;

        public NotesController(AppDbContext db, IWebHostEnvironment environment)
        {
            _db = db;
            _hostingEnvironment = environment;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var notes = await _db.Notes.ToListAsync();

            return new JsonResult(notes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var note = await _db.Notes.FirstOrDefaultAsync(n => n.ID == id);

            return new JsonResult(note);
        }


        [HttpPost]
        public async Task<IActionResult> PostAsync(string location, IFormFile file)
        { 
            Note note =  new Note();
            note.FileName = file.FileName;
            note.ID = Guid.NewGuid();
            note.Location = location;
            note.Date = DateTime.Now;
            note.Title = "foto";
            note.City= "Кременець";
            NoteManager.SaveFileToDatabase(note, _db);
            
            string uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "uploads");
            System.IO.Directory.CreateDirectory(uploads);
            string filePath = Path.Combine(uploads, file.FileName);

            await FileManager.SaveToFolderAsync(filePath, file);

            return new JsonResult(note.ID);
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, Note note)
        {
            var existingNote = await _db.Notes.FirstOrDefaultAsync(n => n.ID == id);
            existingNote.Title = note.Title;
            var success = (await _db.SaveChangesAsync()) > 0;

            return new JsonResult(success);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var note = await _db.Notes.FirstOrDefaultAsync(n => n.ID == id);
            _db.Remove(note);
            var success = (await _db.SaveChangesAsync()) > 0;

            return new JsonResult(success);
        }
    }
}
