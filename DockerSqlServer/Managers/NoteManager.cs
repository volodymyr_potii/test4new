﻿using DockerSqlServer.Models;
using System.Threading.Tasks;

namespace DockerSqlServer.Managers
{
    public class NoteManager
    {
        public static void SaveFileToDatabase(Note note, AppDbContext db)
        {
            db.Notes.Add(note);
            db.SaveChanges();
        }
    }
}
