﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DockerSqlServer.Managers
{
    internal class FileManager
    {
        internal static async Task SaveToFolderAsync(string filePath, IFormFile file)
        {
            using (Stream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
        }
    }
}