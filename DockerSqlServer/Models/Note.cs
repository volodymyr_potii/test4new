﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DockerSqlServer.Models
{
    public class Note
    {
        [Key]
        public Guid ID { get; set; }
        public string FileName { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string  Location { get; set; }
        public string City { get; set; }
    }
}

